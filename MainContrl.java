package Comic.Contrl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import Comic.Datebase.MysqlContrl;
import Comic.File.Etc;
import Comic.File.Image;
import Comic.Web.JsoupPaser;

public class MainContrl extends Thread {
	private JsoupPaser JP = new JsoupPaser();
	private MysqlContrl MC = new MysqlContrl("jdbc:mysql://localhost/comic_db?autoReconnect=true&useSSL=false", "study", "study");
	private Etc ETC = new Etc();
	private Image IMG = new Image();

	private final String TABLE_NAME_COMPLETE = "complete";
	private final String TABLE_NAME_WORKING = "working";
	private final String TABLE_NAME_BAN = "ban";
	private final String TABLE_NAME_CSS = "css";

	private int sleeping_time = 0;

	@Override
	public void run() {
		while (true) {
			if (timeCheckSleep()) {
				try {//6시간+램덤(1분~30분)
					Thread.sleep(sleeping_time);
				} catch (InterruptedException e) {
				}
			} else {
				firstPage();
				for (int k = 0; k < 20; k++) {
					System.out.println("루프 작업 시작");
					loop();
				}
			}
		}
	}

	public void firstPage() {
		System.out.println("첫페이지 작업 시작");
		final String first_page_find_url = "http://www.chuing.net/zboard/zboard.php?id=metc02";
		final String first_page_find_css_link = "body > div:nth-child(25) > form > table > tbody > tr:nth-child(4) > td:nth-child(3) > table > tbody > tr > td > a";

		final String page_list_url_parameter = "http://www.chuing.net/zboard/zboard.php?id=metc02&page=1&m_id=&divpage=4&best=&sn=off&ss=on&sc=on&select_arrange=headnum&desc=asc&no=";
		final String page_list_one_css_text = "table > tbody > tr > td > font";
		final String page_list_one_css_link_parameter = "#pic_top > tbody > tr > td > article";

		MC.setTableName(TABLE_NAME_WORKING);
		MC.readFirstMaxNum();
		if(MC.getDataList().size() == 0){
			MC.setTableName(TABLE_NAME_COMPLETE);
			MC.readFirstMaxNum();	
		}else {
			if(MC.getDataList().get(0)[9].equals("")){
				MC.setTableName(TABLE_NAME_COMPLETE);
				MC.readFirstMaxNum();	
			}
		}

		JP.setUrl(first_page_find_url);
		JP.getPageHtml_GET();

		String first_page_last_url = JP.getCssLink(new String[] { "href" }, first_page_find_css_link).get(0)[0];
		int k = Integer.parseInt(MC.getDataList().get(0)[9]); // db etc 최대갑 가져옴
		int last_num = Integer.parseInt(first_page_last_url.substring(first_page_last_url.lastIndexOf("=") + 1));

		MC.setTableName(TABLE_NAME_WORKING);

		while (last_num != k) {
			System.out.println("페이지 작업");
			JP.setUrl(page_list_url_parameter + (++k));
			JP.getPageHtml_GET();

			if (JP.getCssText(new String[] { page_list_one_css_text }).size() > 0) {
				String[] data = JP.getCssText(new String[] { page_list_one_css_text }).get(0);
				for (String[] Sdata : JP.getCssLink(new String[] { "href" }, page_list_one_css_link_parameter, "a")) {
					MC.write(new String[] { "", Sdata[0], "", data[0], "", "0", "0x01", "", "", k + "", "0", "0" });
					//		System.out.println("첫페이지 주소 =" +Sdata[0]);
				}
			}
		}
		System.out.println("첫 페이지 작업 완료");
	}

	public void loop() {
		MC.setTableName(TABLE_NAME_WORKING);
		MC.deleteOverlap(TABLE_NAME_BAN, TABLE_NAME_COMPLETE);

		MC.readState((byte) 0x1E, (byte) 0x1E); // 최종 작업 완료 되어있는지 확인
		if (MC.getDataList().size() > 0) {// 작업 미완료 피알 있나 보는 것 complete
			LinkedList<String[]> working_datas = MC.getDataList();
			for (String[] working_data : working_datas) {
				int NNO = Integer.parseInt(working_data[0]);
				MC.readNNO(NNO);
				if (MC.getDataList().size() > 0) {
					do {
						MC.setTableName(TABLE_NAME_CSS);
						MC.write(MC.getDataList().get(0));
						MC.setTableName(TABLE_NAME_COMPLETE);
						MC.write(MC.getDataList().get(0));

						MC.setTableName(TABLE_NAME_WORKING);
						NNO = Integer.parseInt(MC.getDataList().get(0)[5]);
						MC.readNNO(NNO);
						if (MC.getDataList().size() == 0) {
							break;
						}
					} while (NNO != 0);
				}
			}
			MC.deleteState((byte) 0x1E);
		}

		MC.readState((byte) 0x1A, (byte) 0x1A); // 최종 작업 완료 되어있는지 확인
		if (MC.getDataList().size() > 0) {// 기존 다운 완료 파일 complete
			LinkedList<String[]> working_datas = MC.getDataList();
			for (String[] working_data : working_datas) {
				int NNO = Integer.parseInt(working_data[0]);
				MC.readNNO(NNO);
				if (MC.getDataList().size() > 0) {
					do {
						MC.setTableName(TABLE_NAME_CSS);
						MC.write(MC.getDataList().get(0));
						MC.setTableName(TABLE_NAME_COMPLETE);
						MC.write(MC.getDataList().get(0));

						MC.setTableName(TABLE_NAME_WORKING);
						NNO = Integer.parseInt(MC.getDataList().get(0)[5]);
						MC.readNNO(NNO);
						if (MC.getDataList().size() == 0) {
							break;
						}
					} while (NNO != 0);
				}
			}
		}

		MC.deleteOverlap(TABLE_NAME_BAN, TABLE_NAME_COMPLETE);
		MC.readState((byte) 0x0A, (byte) 0x02);
		if (MC.getDataList().size() > 0) {// 작업 중인 데이터 마무리 작업
			System.out.println("데이터 작업중");

			LinkedList<String[]> working_datas = MC.getDataList();
			if (MC.readBossTreeNodeCount() < 2) {
				boolean first_page_correct_page_find = false;
				for (String[] working_data : working_datas) {
					System.out.println("페이지 작업 시작 : " + working_data[1]);
					JP.setUrl(working_data[1]);
					if (timeCheckSleep()) {
						try {//6시간+램덤(1분~30분)
							Thread.sleep(sleeping_time);
						} catch (InterruptedException e) {
						}
					}
					if (working_data[7].equals("") || working_data[7].equals("null") || working_data[7].equals("-")) {// data[7] = password
						JP.getPageHtml_GET();
						System.out.println("페이지 접속");
					} else {
						Map<String, String> password_parameter = new HashMap<String, String>();
						password_parameter.put(working_data[8], working_data[7]);
						JP.getPageHtml_POST(password_parameter);
					}

					boolean title_boolean = false;
					boolean img_boolean = false;
					boolean similar_title = false;
					LinkedList<String[]> title_datas = null;
					LinkedList<String[]> img_datas = null;
					LinkedList<String[]> sun_urls = null;
					String result_title_css = "";

					//css 사용 하지 말고 밴 활용하여서 작업범위 줄이기
					title_datas = JP.getCssText(new String[] { "a", "title" });//추가 
					img_datas = JP.getCssLink(new String[] { "data-src", "src" }, "img");//추가
					for (String[] title_data : title_datas) {
						if (ETC.regularTitle(title_data[0], working_data[3])) {
							title_boolean = true;
							result_title_css = title_data[1];
							break;
						}
					}
					for (String[] title_data : title_datas) {
						if (similar_title = ETC.similarTitle(title_data[0])) {
							break;
						}
					}

					MC.setTableName(TABLE_NAME_WORKING);
					IMG.setImageList(img_datas);
					img_boolean = IMG.imageSizeChecking();

					if (!first_page_correct_page_find & similar_title & title_boolean & img_boolean) {// 만화책 페이지 찾음 complete
						System.out.println("111 만화책 페이지 발견");
						// 만화책 다운 및 웹 드라이브 자동 업로드  만화책 다운.complete
						IMG.setFolderName(working_data[3]);
						IMG.imageSizeCheckingDownload();
						first_page_correct_page_find = true;
						working_data[4] = result_title_css;
						working_data[6] = "0x1E";
						MC.reWrite(working_data);
					} else if (similar_title & title_boolean & !img_boolean) {//자식 링크 탐색 complete // jp 에서 자식 링크 데이터 못 읽어옴
						System.out.println("110 자식링크 탐색");
						working_data[4] = result_title_css;
						working_data[6] = "0x0A";
						if (JP.getPassWordPageFinder()) {
							working_data[7] = "suspicion";
							MC.reWrite(working_data);
						} else {
							MC.reWrite(working_data);
							if (!first_page_correct_page_find) {//first_page_correct_page_find 찾았는데도 다른 만화책 없나 뒤지는 과정에서 발생하는 자식 페이지 생성 금지
								sun_urls = JP.getCssLink(new String[] { "href" }, "a");
								for (String[] sun_url : sun_urls) {
									MC.write(new String[] { "", sun_url[0], "", working_data[3], "", working_data[0], "0x02", "", "", "", (Integer.parseInt(working_data[10]) + 1) + "" });
								}
								MC.deleteOverlap(TABLE_NAME_BAN, TABLE_NAME_COMPLETE);
							}
						}
					} else if (!similar_title & !title_boolean & !img_boolean) {// 01 광고 페이지 작업 넘김 // 00 작업 넘김 - ban에 추가 complete
						System.out.println("000 광고 페이지");
						working_data[6] = "0x0A";
						MC.reWrite(working_data);
						MC.setTableName(TABLE_NAME_BAN);
						MC.write(working_data);
					} else if (similar_title & !title_boolean & img_boolean) { // css 불러와서 작업 할 페이지 
						System.out.println("101 css에서 가져올 페이지");
						MC.setTableName(TABLE_NAME_CSS);
						MC.readCssMemory(working_data[1]);
						LinkedList<String[]> memery_css = MC.getDataList();
						if (memery_css.size() > 0) {
							title_datas = JP.getCssText(new String[] { memery_css.get(0)[4] });
							for (String[] title_data : title_datas) {
								if (similar_title = ETC.similarTitle(title_data[0])) {
									working_data[3] = title_data[0];
									break;
								}
							}
							IMG.setFolderName(working_data[3]);
							IMG.imageSizeCheckingDownload();
							working_data[6] = "0x1A";
							MC.setTableName(TABLE_NAME_WORKING);
							MC.reWrite(working_data);
							MC.setTableName(TABLE_NAME_COMPLETE);
							MC.write(working_data);
						}
					} else if (similar_title & !title_boolean & !img_boolean) {//중간 페이지
						System.out.println("100 자식링크 탐색");
						working_data[4] = result_title_css;
						working_data[6] = "0x0A";
						if (JP.getPassWordPageFinder()) {
							working_data[7] = "suspicion";
							MC.reWrite(working_data);
						} else {
							MC.reWrite(working_data);
							sun_urls = JP.getCssLink(new String[] { "href" }, "a");
							for (String[] sun_url : sun_urls) {
								MC.write(new String[] { "", sun_url[0], "", working_data[3], "", working_data[0], "0x02", "", "", "", (Integer.parseInt(working_data[10]) + 1) + "" });
							}
							MC.deleteOverlap(TABLE_NAME_BAN, TABLE_NAME_COMPLETE);
						}
					} else { // 중간 페이지도 통과
						System.out.println("페이지 통과");
						working_data[6] = "0x0A";
						MC.reWrite(working_data);
					}
					System.out.println("페이지 작업 완료");
				}
			} else {//아무리 돌려도 못찾음 
				MC.deleteState((byte) 0x02);
			}
		} else {// 츄잉 데이터에서 저장 한 0000001 중 하나를 추출 하여 작업 해야 함 . 아마 complete
			MC.deleteOverlap(TABLE_NAME_BAN, TABLE_NAME_COMPLETE);
			MC.readState((byte) 0x01, (byte) 0x01);
			if (MC.getDataList().size() > 0) {// 작업 중인 데이터 마무리 작업
				System.out.println("완료 데이터 마무리");
				String[] data = MC.getDataList().get(0);
				data[6] = "0x03";
				MC.reWrite(data);
			}
		}
	}

	private boolean timeCheckSleep() {
		final String START_SLEEP_TIME_HOUR_FIRST = "00";//잠들 시간(6시간)

		final String START_SLEEP_TIME_HOUR_SECOND = "12";//잠들 시간(1시간)
		final String START_SLEEP_TIME_HOUR_THIRD = "18";//잠들 시간(1시간)

		SimpleDateFormat hourTimeFormat = new SimpleDateFormat("HH");
		long currentHourTime = 0, sleepHourTime = 0;
		try {
			currentHourTime = hourTimeFormat.parse(hourTimeFormat.format(new Date())).getTime();
			sleepHourTime = hourTimeFormat.parse(START_SLEEP_TIME_HOUR_FIRST).getTime();
		} catch (ParseException e) {
		}

		if (currentHourTime == sleepHourTime) {
			sleeping_time = (int) (21600000 + ((Math.random() * 30) * 60000));
			System.out.println("6시간 쉴거야");
			return true;
		} else {
			try {
				sleepHourTime = hourTimeFormat.parse(START_SLEEP_TIME_HOUR_SECOND).getTime();
			} catch (ParseException e) {
			}
		}

		if (currentHourTime == sleepHourTime) {
			sleeping_time = (int) (2000000 + ((Math.random() * 30) * 60000));
			System.out.println("33~60분 쉴거야");
			return true;
		} else {
			try {
				sleepHourTime = hourTimeFormat.parse(START_SLEEP_TIME_HOUR_THIRD).getTime();
			} catch (ParseException e) {
			}
		}

		if (currentHourTime == sleepHourTime) {
			sleeping_time = (int) (2000000 + ((Math.random() * 30) * 60000));
			System.out.println("33~60분 쉴거야");
			return true;
		} else {
			return false;
		}

	}
}

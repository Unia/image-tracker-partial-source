package Comic.Datebase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;

public class MysqlContrl extends Comic.Datebase.Mysql {
	private LinkedList<String[]> dataList = null;
	private Connection conn = null;
	private Statement stmt = null;

	public MysqlContrl(String mysql_main_url, String mysql_table_id, String mysql_table_pw) {
		setMsyqlMainUrl(mysql_main_url);
		setMysqlTableId(mysql_table_id);
		setMysqlTablePw(mysql_table_pw);
	}

	public LinkedList<String[]> getDataList() {
		return dataList;
	}

	private void createStatement() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			conn = DriverManager.getConnection(getMsyqlMainUrl(), getMysqlTableId(), getMysqlTablePw());
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e1) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e1) {
			}
		}
	}

	private void readStatement(ResultSet rs) {
		dataList = new LinkedList<String[]>();
		try {
			if (rs != null) {
				while (rs.next()) {
					String[] data = new String[] { rs.getInt("NNO") + "", rs.getString("LINK"), rs.getString("LINKCSS"), rs.getString("TITLE"), rs.getString("TITLECSS"), rs.getInt("BOSS") + "",
							rs.getByte("STATE") + "", rs.getString("PASSWORD"), rs.getString("PASSWORDTAG"), rs.getString("ETC"), rs.getInt("DEPTH") + "" };
					dataList.add(data);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
			}
		}
	}

	public void createTable() {
		try {
			createStatement();
			stmt.executeUpdate("create table " + getTableName()
					+ " (NNO integer(10) auto_increment PRIMARY KEY comment '넘버',LINK varchar(300) UNIQUE KEY comment '링크',LINKCSS varchar(300) comment '링크',TITLE varchar(300) comment '링크',TITLECSS varchar(300) comment 'TITLECSS',BOSS integer(10) comment '윗테크',STATE BIT(7) comment '상태',PASSWORD varchar(300) comment '패스워드',PASSWORDTAG varchar(300) comment '패스워드 태그',ETC varchar(300) comment 'ETC',DEPTH integer(10) comment '깊이')comment 'COMIC_LINK'");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void write(String[] data) {// 데이터 받아오는거 추가할것
		try {
			createStatement();
			stmt.executeUpdate("INSERT INTO " + getTableName() + " (LINK,LINKCSS,TITLE,TITLECSS,BOSS,STATE,PASSWORD,PASSWORDTAG,ETC,DEPTH) values ('" + data[1] + "','" + data[2] + "','" + data[3]
					+ "','" + data[4] + "'," + data[5] + "," + data[6] + ",'" + data[7] + "','" + data[8] + "','" + data[9] + "'," + data[10] + ")");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void reWrite(String[] data) {
		try {
			createStatement();
			stmt.executeUpdate("UPDATE " + getTableName() + " SET LINK='" + data[1] + "',LINKCSS='" + data[2] + "',TITLE='" + data[3] + "',TITLECSS='" + data[4] + "',BOSS='" + data[5] + "',STATE="
					+ data[6] + ",PASSWORD='" + data[7] + "',PASSWORDTAG='" + data[8] + "',ETC='" + data[9] + "',DEPTH='" + data[10] + "' WHERE NNO=" + data[0]);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void readAll() {// 데이터 받아오는거 추가할것
		try {
			createStatement();
			readStatement(stmt.executeQuery("select * from " + getTableName()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void readNNO(int NNO) {// 데이터 받아오는거 추가할것
		try {
			createStatement();
			readStatement(stmt.executeQuery("select * from " + getTableName() + " where NNO = " + NNO));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void readState(byte extraction_state, byte result_state) {
		try {
			createStatement();
			readStatement(stmt.executeQuery("SELECT * from " + getTableName() + " where (" + getTableName() + ".STATE & " + extraction_state + ") = " + result_state));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void readCssMemory(String link) { // 기억자 css 데이터 디비 참조 //TODO: 디버깅
		try {
			createStatement();
			String[] separation_link = link.split("/");
			ResultSet rs = null;
			for (int k = separation_link.length; k > 3; k--) {//http://로 찾으면 안되기 땜에 3이상
				rs = stmt.executeQuery("select *, max(r) from (SELECT *, count(TITLECSS) as r FROM " + getTableName() + " where " + getTableName() + ".LINK = '" + link
						+ "' group by TITLECSS order by r desc)as temp");
				rs.next();
				if (rs.getInt("NNO") > 0) {
					break;
				} else {
					link = link.replace("/" + separation_link[k - 1], "");
				}
			}
			readStatement(rs);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void readFirstMaxNum() { // first(츄잉) 페이지 가장 마지막 데이터 정보
		try {
			createStatement();
			readStatement(stmt.executeQuery("select * from " + getTableName() + " where ETC = (select max(ETC) from " + getTableName() + ")"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public int readBossTreeNodeCount() {
		int result = 6;
		try {
			createStatement();
			ResultSet rs = stmt.executeQuery("select max(DEPTH) from " + getTableName());
			rs.next();
			result = rs.getInt("max(DEPTH)");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		return result;
	}

	public void deleteOverlap(String banTableName, String completeTableName) {
		try {
			createStatement();
			stmt.executeUpdate("DELETE FROM " + getTableName() + " WHERE " + getTableName() + ".LINK IN (SELECT LINK FROM(SELECT " + getTableName() + ".LINK FROM " + getTableName() + " inner JOIN "
					+ banTableName + " ON " + getTableName() + ".LINK LIKE CONCAT(" + banTableName + ".LINK,'%')) tmp)");
			stmt.executeUpdate("DELETE FROM " + getTableName() + " WHERE " + getTableName() + ".LINK IN (SELECT LINK FROM(SELECT " + getTableName() + ".LINK FROM " + getTableName() + " inner JOIN "
					+ completeTableName + " ON " + getTableName() + ".LINK LIKE CONCAT(" + completeTableName + ".LINK,'%')) tmp)");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}

	public void deleteState(byte state) {
		try {
			createStatement();
			stmt.executeUpdate("DELETE FROM " + getTableName() + " WHERE " + getTableName() + ".LINK IN (SELECT LINK FROM(SELECT LINK from " + getTableName() + " where (" + getTableName()
					+ ".STATE & " + state + ") = " + state + ") tmp)");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
	}
}
